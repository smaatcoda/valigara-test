<?php

namespace Valigara\MWS;

use Valigara\MWS\Entities\Buyer;
use Valigara\MWS\Entities\Order;
use Valigara\MWS\Interfaces\IOutbondShipping;

class FulfillmentOutboundShipment implements IOutbondShipping
{
    /**
     * @var string MWS Api resource
     */
    public const API_ENDPOINT = 'FulfillmentOutboundShipment';

    /**
     * @var Connection MWS API HTTP client preset
     */
    protected Connection $connection;

    /**
     * @param Connection MWS API HTTP client preset
     */
    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * Sends command to Amazon FBA to ship order
     * @param Order $order
     * @param Buyer $buyer
     * @return array|string
     * @throws Exceptions\Exception
     * @throws Exceptions\InvalidPayloadException
     * @throws Exceptions\NotFoundException
     */
    public function ship(Order $order, Buyer $buyer)
    {
        $parameters = [
            'MWSAuthToken' => 'amzn.mws.4ea38b7b-f563-7709-4bae-87aeaEXAMPLE',
            'Marketplace' => 'ATExampleER',
            'SellerId' => 'A1ExampleE6',
            'AWSAccessKeyId' => '0PExampleR2',
            'SignatureMethod' => 'HmacSHA256',
            'SignatureVersion' => '2',
            'Timestamp' => (new \DateTime())->format(DATE_ATOM),
            'Version' => '2010-10-01',

            'Action' => 'CreateFulfillmentOrder',
            'FulfillmentAction' => 'ship',
            'ShippingSpeedCategory' => 'Standard',
            'DisplayableOrderComment' => 'Test order',
            'DestinationAddress' => $this->getAddress($buyer)
        ];

        foreach ($order->data['products'] as $product) {
            $parameters['Items'][] = $this->getCreateFulfillmentOrderItem($product);
        }

        return $this->connection->call('post', self::API_ENDPOINT, $parameters);
    }

    /**
     * Formats generic order products data according to CreateFulfillmentOrderItem datatype
     *
     * @see http://docs.developer.amazonservices.com/en_US/fba_outbound/FBAOutbound_Datatypes.html#CreateFulfillmentOrderItem
     * @param array $productData
     * @return array
     */
    private function getCreateFulfillmentOrderItem(array $productData)
    {
        return [
            'SellerSKU' => $productData['sku'] ?? '',
            'SellerFulfillmentOrderItemId' => $productData['order_product_id'] ?? '',
            'Quantity' => $productData['amount'] ?? '',
            'PerUnitPrice' => $productData['buying_price'] ?? '',
        ];
    }

    /**
     * Formats generic buyer data according to Address datatype
     *
     * @see http://docs.developer.amazonservices.com/en_US/fba_outbound/FBAOutbound_Datatypes.html#Address
     * @param Buyer $buyer
     * @return array
     */
    private function getAddress(Buyer $buyer)
    {
        return [
            'Name' => $buyer->name ?? '',
            'Line1' => $buyer->address ?? '',
            'StateOrProvinceCode' => $buyer->state ?? '',
            'CountryCode' => $buyer->get_country_code() ?? '',
        ];
    }
}
