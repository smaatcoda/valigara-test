<?php

namespace Valigara\MWS;

class MWSFactory
{
    /**
     * @var array
     */
    protected array $config;

    /**
     * MWSFactory constructor.
     * @param array $config
     */
    public function __construct(array $config)
    {
        $this->config = $config;
    }

    /**
     * Instantiates an MWS resource repository/service
     *
     * @param $repositoryClass
     * @return mixed
     */
    public function make($repositoryClass)
    {
        $connection = new Connection($this->config['base_url'] ?? '', $this->config['secret_key'] ?? '');

        return new $repositoryClass($connection);
    }
}
