<?php


namespace Valigara\MWS;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use Valigara\MWS\Exceptions\Exception;
use Valigara\MWS\Exceptions\InvalidPayloadException;
use Valigara\MWS\Exceptions\NotFoundException;

class Connection
{
    /**
     * @var string
     */
    protected string $baseUrl;

    /**
     * @var string
     */
    protected string $secretKey;

    /**
     * @param $baseUrl
     * @param $secretKey
     */
    public function __construct($baseUrl, $secretKey)
    {
        $this->baseUrl = $baseUrl;
        $this->secretKey = $secretKey;
    }


    /**
     * Performs a HTTP request to Amazon MWS
     *
     * @param string $method HTTP method
     * @param string $endpoint MWS resource
     * @param array $parameters Resource data
     *
     * @return array
     * @throws Exception
     * @throws InvalidPayloadException
     * @throws NotFoundException
     */
    public function call(string $method, string $endpoint, array $parameters = [])
    {
        $httpClient = new Client(['base_uri' => "https://{$this->baseUrl}/"]);

        $parameters['SignatureMethod'] = 'HmacSHA256';
        $parameters['SignatureVersion'] = '2';
        $parameters['Signature'] = $this->generateSignature($method, $endpoint, $parameters);

        $queryString = $this->buildQuery($parameters);

        $url = "$endpoint?$queryString";

        try {
            $response = $httpClient->request($method, $url);

            return $this->xmlToArray($response->getBody()->getContents());
        } catch (BadResponseException $exception) {
            $errors = null;
            $details = [
                'method' => $method,
                'url' => $url,
                'response' => $exception->getResponse(),
                'message' => $exception->getMessage(),
            ];

            if ($exception->getResponse()) {
                $errors = $this->xmlToArray($this->getErrorData($exception));
                $details['response'] = $errors;
                $details['message'] = $this->getErrorMessage($exception);
            }

            switch ($exception->getCode()) {
                case 404:
                    throw new NotFoundException($errors, $details, $details['message'], $exception->getCode(),
                        $exception);
                case 422:
                    throw new InvalidPayloadException($errors, $details, $details['message'], $exception->getCode(),
                        $exception);
                default:
                    throw new Exception($errors, $details, $details['message'], $exception->getCode(), $exception);
            }
        }
    }

    /**
     * Generates a signature for api request
     *
     * @param string $method HTTP method
     * @param string $endpoint MWS resource
     * @param array $parameters Resource data
     * @return string
     */
    private function generateSignature(string $method, string $endpoint, array $parameters): string
    {
        // Sort parameters alphabetically
        sort($parameters);

        $queryString = $this->buildQuery($parameters);

        $query = strtoupper($method) . "\n";
        $query .= $this->baseUrl . "\n";
        $query .= $endpoint . "\n";
        $query .= $queryString . "\n";

        $signature = hash_hmac('sha256', $query, $this->secretKey);

        return base64_encode($signature);
    }

    /**
     * Joins array data into a query string
     *
     * @param array $params
     * @return string
     */
    private function buildQuery(array $params): string
    {
        $queryString = http_build_query($params);

        /**
         * Do not URL encode any of the unreserved characters that RFC 3986 defines
         * @see http://docs.developer.amazonservices.com/en_US/dev_guide/DG_QueryString.html
         */
        return str_replace(array('+', '*', '%7E'), array('%20', '%2A', '~'), $queryString);
    }

    /**
     * Converts xml to array
     *
     * @param $xmlstring
     * @return array
     */
    private function xmlToArray($xmlstring): array
    {
        if (empty($xmlstring)) {
            return [];
        }

        $xml = simplexml_load_string($xmlstring, null, LIBXML_NOCDATA);
        $json = json_encode($xml, JSON_THROW_ON_ERROR, 512);

        return json_decode($json, true, 512, JSON_THROW_ON_ERROR);
    }


    /**
     * Retrieves full (not truncated) error message from an MWS API exception
     *
     * @param BadResponseException $exception
     * @return string
     */
    public function getErrorMessage(BadResponseException $exception): string
    {
        if ($exception->getResponse() === null) {
            return $exception->getMessage();
        }

        $level = (int)floor($exception->getResponse()->getStatusCode() / 100);
        if ($level === 4) {
            $label = 'Client error';
        } elseif ($level === 5) {
            $label = 'Server error';
        } else {
            $label = 'Unsuccessful request';
        }

        $uri = $exception->getRequest()->getUri();
        $userInfo = $uri->getUserInfo();

        if (false !== ($pos = strpos($userInfo, ':'))) {
            $uri = $uri->withUserInfo(substr($userInfo, 0, $pos), '***');
        }

        // Client Error: `GET /` resulted in a `404 Not Found` response:
        // <html> ... (truncated)
        $message = sprintf(
            '%s: `%s %s` resulted in a `%s %s` response',
            $label,
            $exception->getRequest()->getMethod(),
            $uri,
            $exception->getResponse()->getStatusCode(),
            $exception->getResponse()->getReasonPhrase()
        );

        // Reset body reader pointer, otherwise getContents() returns null, because of being read previously
        $exception->getResponse()->getBody()->rewind();
        $message .= ":\n{$exception->getResponse()->getBody()->getContents()}\n";

        return $message;
    }

    /**
     * Retrieves error data from an MWS API exception
     *
     * @param BadResponseException $exception
     * @return string
     */
    public function getErrorData(BadResponseException $exception): string
    {
        if ($exception->getResponse() === null) {
            return '';
        }

        // Reset body reader pointer, otherwise getContents() returns null, because of being read previously
        $exception->getResponse()->getBody()->rewind();

        return $exception->getResponse()->getBody()->getContents();
    }
}
