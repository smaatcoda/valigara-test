<?php

namespace Valigara\MWS\Exceptions;

use Valigara\MWS\Exceptions\Exception;

class NotFoundException extends Exception
{

}