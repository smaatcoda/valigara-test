<?php

namespace Valigara\MWS\Exceptions;

use Exception as BaseException;

class Exception extends BaseException
{
    /**
     * @var array
     */
    protected array $errors;

    /**
     * @var array
     */
    protected array $details;

    /**
     * Exception constructor.
     *
     * @param array $errors
     * @param array $details
     * @param string $message
     * @param int $code
     * @param BaseException $previous
     */
    public function __construct(
        array $errors = [],
        array $details = [],
        string $message = '',
        int $code = 0,
        BaseException $previous = null
    ) {
        $this->errors = $errors;
        $this->details = $details;

        parent::__construct($message, $code, $previous);
    }

    /**
     * @return array
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    /**
     * @return array
     */
    public function getDetails(): array
    {
        return $this->details;
    }

}