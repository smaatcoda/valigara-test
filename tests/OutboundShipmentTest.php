<?php

namespace Valigara\MWS\Tests;

use PHPUnit\Framework\TestCase;
use Valigara\MWS\Connection;
use Valigara\MWS\Entities\MockBuyer;
use Valigara\MWS\Entities\MockOrder;
use Valigara\MWS\Exceptions\Exception;
use Valigara\MWS\FulfillmentOutboundShipment;
use Valigara\MWS\MWSFactory;

class OutboundShipmentTest extends TestCase
{

    public function test_connection()
    {
        $parameters = [
            'AWSAccessKeyId' => '0PExampleR2',
            'Action' => 'GetFeedSubmissionResult',
            'FeedSubmissionId' => '20Example76',
            'MWSAuthToken' => 'amzn.mws.4ea38b7b-f563-7709-4bae-87aeaEXAMPLE',
            'Marketplace' => 'ATExampleER',
            'SellerId' => 'A1ExampleE6',
            'SignatureMethod' => 'HmacSHA256',
            'SignatureVersion' => '2',
            'Timestamp' => (new \DateTime())->format(DATE_ATOM),
            'Version' => '2009-01-01',
        ];

        $connection = new Connection('mws.amazonservices.com', 'dummykey');

        try {
            $connection->call('post', 'Feeds', $parameters);
        } catch (\Exception $exception) {
            $this->assertInstanceOf(Exception::class, $exception);
            /** @var Exception $exception */
            $this->assertArrayHasKey('Error', $exception->getErrors());
            $this->assertEquals('The AWS Access Key Id you provided does not exist in our records.', $exception->getErrors()['Error']['Message']);
        }
    }

    public function test_outbound_shipment_service()
    {
        $factory = new MWSFactory(['base_url' => 'mws.amazonservices.com', 'secret_key' => 'dummykey']);
        /** @var FulfillmentOutboundShipment $shipmentService */
        $shipmentService = $factory->make(FulfillmentOutboundShipment::class);

        $buyer = new MockBuyer();
        $order = new MockOrder();
        $order->load();

        try {
            $shipmentService->ship($order, $buyer);
        } catch (\Exception $exception) {
            $this->assertInstanceOf(Exception::class, $exception);
            /** @var Exception $exception */
            $this->assertArrayHasKey('Error', $exception->getErrors());
//            $this->assertEquals('The AWS Access Key Id you provided does not exist in our records.', $exception->getErrors()['Error']['Message']);
        }

    }

}